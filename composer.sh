#! /bin/bash

function fixFileOwnership () {
    sudo chown -R ${USER} vendor
}

function optimizeAutoload () {
    docker container exec -it swoft-srv composer dump-autoload -o
}

function runComposerCommand () {
    docker container exec -it swoft-srv composer $@
}

runComposerCommand "$@"
optimizeAutoload
fixFileOwnership